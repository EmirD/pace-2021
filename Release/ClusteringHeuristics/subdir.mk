################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../ClusteringHeuristics/cluster_editing_heuristic_solver.cpp \
../ClusteringHeuristics/directly_hashed_cluster_edge_node_weight.cpp \
../ClusteringHeuristics/directly_hashed_integer_map.cpp \
../ClusteringHeuristics/directly_hashed_integer_set.cpp \
../ClusteringHeuristics/graph.cpp \
../ClusteringHeuristics/main.cpp \
../ClusteringHeuristics/parameter_handler.cpp \
../ClusteringHeuristics/solver_state.cpp 

OBJS += \
./ClusteringHeuristics/cluster_editing_heuristic_solver.o \
./ClusteringHeuristics/directly_hashed_cluster_edge_node_weight.o \
./ClusteringHeuristics/directly_hashed_integer_map.o \
./ClusteringHeuristics/directly_hashed_integer_set.o \
./ClusteringHeuristics/graph.o \
./ClusteringHeuristics/main.o \
./ClusteringHeuristics/parameter_handler.o \
./ClusteringHeuristics/solver_state.o 

CPP_DEPS += \
./ClusteringHeuristics/cluster_editing_heuristic_solver.d \
./ClusteringHeuristics/directly_hashed_cluster_edge_node_weight.d \
./ClusteringHeuristics/directly_hashed_integer_map.d \
./ClusteringHeuristics/directly_hashed_integer_set.d \
./ClusteringHeuristics/graph.d \
./ClusteringHeuristics/main.d \
./ClusteringHeuristics/parameter_handler.d \
./ClusteringHeuristics/solver_state.d 


# Each subdirectory must supply rules for building sources it contributes
ClusteringHeuristics/%.o: ../ClusteringHeuristics/%.cpp ClusteringHeuristics/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -Ofast -O3 -Wall -c -fmessage-length=0 -flto -DNDEBUG -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


