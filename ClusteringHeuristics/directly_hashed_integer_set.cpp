#include "directly_hashed_integer_set.h"
#include "runtime_assert.h"

DirectlyHashedIntegerSet::DirectlyHashedIntegerSet(int num_values) :
	value_to_location_(num_values, -1)
{
}

void DirectlyHashedIntegerSet::Insert(int value)
{
	if (!IsPresent(value))
	{
		value_to_location_[value] = int(present_values_.size());
		present_values_.push_back(value);
	}
}

void DirectlyHashedIntegerSet::Grow()
{
	value_to_location_.push_back(-1);
}

void DirectlyHashedIntegerSet::Resize(int new_size)
{
	runtime_assert(new_size >= GetCapacity());
	while (GetCapacity() < new_size) { Grow(); }
}

void DirectlyHashedIntegerSet::Clear()
{
	for (int value : present_values_) 
	{ 
		value_to_location_[value] = -1;
	}
	present_values_.clear();

}

void DirectlyHashedIntegerSet::Remove(int value)
{
	runtime_assert(IsPresent(value));

	present_values_[value_to_location_[value]] = present_values_.back();
	value_to_location_[present_values_.back()] = value_to_location_[value];
	value_to_location_[value] = -1;
	present_values_.pop_back();
}

bool DirectlyHashedIntegerSet::IsPresent(int value) const
{
	return value_to_location_[value] != -1;
}

int DirectlyHashedIntegerSet::GetNumPresentValues() const
{
	return int(present_values_.size());
}

int DirectlyHashedIntegerSet::GetCapacity() const
{
	return int(value_to_location_.size());
}

typename std::vector<int>::const_iterator DirectlyHashedIntegerSet::begin() const
{
	return present_values_.begin();
}

typename std::vector<int>::const_iterator DirectlyHashedIntegerSet::end() const
{
	return present_values_.end();
}

