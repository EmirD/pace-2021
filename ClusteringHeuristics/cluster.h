#pragma once

struct Cluster
{
	explicit Cluster(int id) :id(id) {}

	bool operator==(Cluster c) const { return this->id == c.id; }
	bool operator!=(Cluster c) const { return this->id != c.id; }
	int id;
};