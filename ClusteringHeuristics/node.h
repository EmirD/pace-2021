#pragma once

struct Node
{
	explicit Node(int id) :id(id) {}

	bool operator==(Node n) const { return this->id == n.id; }
	bool operator!=(Node n) const { return this->id != n.id; }

	int id;
};