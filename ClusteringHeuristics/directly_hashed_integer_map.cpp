#include "directly_hashed_integer_map.h"
#include "runtime_assert.h"

#include <stdint.h>

const int DirectlyHashedIntegerMap::NOT_PRESENT = INT32_MIN;

DirectlyHashedIntegerMap::DirectlyHashedIntegerMap(int size):
	present_keys_(),
	keys_to_values_(size, NOT_PRESENT)
{
}

int& DirectlyHashedIntegerMap::operator[](int key)
{
	if (!HasKey(key)) 
	{ 
		keys_to_values_[key] = 0; 
		present_keys_.push_back(key);
	}

	return keys_to_values_[key];
}

void DirectlyHashedIntegerMap::Resize(int new_size)
{
	runtime_assert(GetNumKeysPresent() == 0);
	keys_to_values_.resize(new_size, NOT_PRESENT);	
}

void DirectlyHashedIntegerMap::Clear()
{
	for (int key : present_keys_) { keys_to_values_[key] = NOT_PRESENT; }
	present_keys_.clear();
}

bool DirectlyHashedIntegerMap::HasKey(int key) const
{
	return keys_to_values_[key] != NOT_PRESENT;
}

int DirectlyHashedIntegerMap::GetNumKeysPresent() const
{
	return int(present_keys_.size());
}

bool DirectlyHashedIntegerMap::IsEmpty() const
{
	return GetNumKeysPresent() == 0;
}

const std::vector<int>& DirectlyHashedIntegerMap::GetKeys() const
{
	return present_keys_;
}
