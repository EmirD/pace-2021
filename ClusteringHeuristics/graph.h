#pragma once

#include "node.h"

#include <vector>
#include <string>

struct PairNodeEdgeWeight { Node node; int edge_weight; PairNodeEdgeWeight(Node n, int ew):node(n), edge_weight(ew) {}; };

class Graph
{
public:
	Graph();
	//read the graph given in the Pace Challenge 2021 format (DIMACS)
	//todo description
	Graph(std::string file_location, bool read_from_standard_input);
	Graph(const std::vector<std::vector<PairNodeEdgeWeight> >& adjacency_lists, const std::vector<int>& node_weights);

	bool operator==(Graph& rhs);

	int NumNodes() const;
	int64_t NumEdges() const;
	int GetNodeWeight(Node node) const;

	void SortNeighboursByID(Node node);
	const std::vector<PairNodeEdgeWeight>& GetAdjacencyListForNode(Node node);

	bool AreNeighbours(Node node1, Node node2) const; //used only for debug purposes, should be normally avoided because it may be expensive compared to iterating over neighbours
	int GetEdgeCost(Node node1, Node node2) const; //used only for debug purposes, should be normally avoided because it may be expensive compared to iterating over neighbours
		
private:
	std::vector<int> node_weights_; //[i] is the weight of the i-th node	
	std::vector<std::vector<PairNodeEdgeWeight> > adjacency_lists_; //[i] is the list of neighbouring nodes and their weight of the i-th node
};