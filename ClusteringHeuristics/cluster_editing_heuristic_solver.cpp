#include "cluster_editing_heuristic_solver.h"
#include "node.h"
#include "cluster.h"
#include "runtime_assert.h"

#include <iostream>
#include <algorithm>
#include <set>

ClusterEditingHeuristicSolver::ClusterEditingHeuristicSolver(ParameterHandler& parameters):
	stopwatch_(parameters.GetIntegerParameter("time")),
	state_(parameters.GetStringParameter("file"), parameters.GetBooleanParameter("PACE2021-input")),
	evaluated_clusters_(0),
	local_cluster_node_weights2_(0),
	local_edge_weights2_(0),
	best_cost_(INT32_MAX),
	freeze_best_solution_(false),
	randomise_sequence_(parameters.GetBooleanParameter("in-order")),
	seed_(parameters.GetIntegerParameter("seed")),
	num_iterations_total_(0)
{
	std::string verbosity = parameters.GetStringParameter("verbosity");
	if (verbosity == "PACE2021") { verbosity_ = Verbosity::PACE2021; }
	else if (verbosity == "limited") { verbosity_ = Verbosity::LIMITED; }
	else if (verbosity == "detailed") { verbosity_ = Verbosity::DETAILED; }
	else { std::cout << "Error: Verbosity level \"" << verbosity << "\" not recognised!\n"; exit(1); }
	
	if (seed_ >= 0) { random_number_generator_.seed(seed_); }
	if (verbosity_ >= Verbosity::LIMITED) { std::cout << "num nodes: " << state_.graph_original_.NumNodes() << "; num edges: " << state_.graph_original_.NumEdges() << "\n"; }
}

ParameterHandler ClusterEditingHeuristicSolver::CreateParameterHandler()
{
	ParameterHandler parameters;

	parameters.DefineNewCategory("General Parameters");

	parameters.DefineStringParameter
	(
		"file",
		"Location of the instance file given in DIMACS graph format.",
		"", //default value
		"General Parameters"
	);

	parameters.DefineBooleanParameter
	(
		"PACE2021-input",
		"If set, reads the input file using standard input rather than a file. Used in the PACE2021 Challenge.",
		false,
		"General Parameters"
	);

	parameters.DefineIntegerParameter
	(
		"time",
		"Time limit given in seconds.",
		86400, //default value, one day
		"General Parameters",
		0.0 //min_value
	);

	parameters.DefineIntegerParameter
	(
		"seed",
		"Seed for the random number generator. Influences the order in which nodes are considered in the algorithm and sideway move acceptance.",
		5, //default value
		"General Parameters",
		0 //min_value
	);

	parameters.DefineStringParameter
	(
		"verbosity",
		"Controls the level of detail that is printed by the solver.",
		"detailed",
		"General Parameters",
		{ "PACE2021", "limited", "detailed" }
	);

	parameters.DefineBooleanParameter
	(
		"in-order",
		"Instructs the solver to investigate nodes in order of the index as given in the instance. Normally nodes are randomly visited. This is mainly for experimental purposes.",
		false,
		"General Parameters"
	);

	return parameters;
}

ClusterEditingSolution ClusterEditingHeuristicSolver::Solve()
{
	Initialise();

	std::vector<Node> nodes;
	int num_merges = 0;

	while (stopwatch_.IsWithinTimeLimit())
	{
		int64_t cost_before_merging = state_.current_cost_;
		while (stopwatch_.IsWithinTimeLimit())
		{
			int64_t cost_before_local_search = state_.current_cost_;
			int64_t num_moves_improving = 0;
			int64_t num_moves_sideway = 0;
			nodes.clear();
			for (int i = 0; i < state_.graph_.NumNodes(); i++) { nodes.push_back(Node(i)); }
			if (randomise_sequence_) { std::shuffle(nodes.begin(), nodes.end(), random_number_generator_); }

			int64_t old_objective = state_.current_cost_;

			for (Node current_node : nodes)
			{
				Cluster best_cluster = ComputeBestLocalClusterAssignmentForNodeBetter(current_node);
				int64_t temp = state_.current_cost_;
				bool changing_clusters = state_.GetClusterAssignmentForNode(current_node) != best_cluster;
				state_.AssignClusterToNode(current_node, best_cluster);

				runtime_assert(temp >= state_.current_cost_);
				num_moves_improving += (temp > state_.current_cost_);
				num_moves_sideway += (temp == state_.current_cost_ && changing_clusters);
			}

			num_iterations_total_++;
			if (cost_before_local_search == state_.current_cost_) { break; } //no progress made

			UpdateBestSolution();

			if (verbosity_ >= Verbosity::DETAILED) { std::cout << "num sideway moves: " << num_moves_sideway << std::endl << "num improving moves: " << num_moves_improving << std::endl; }
		}		
		
		if (cost_before_merging == state_.current_cost_) { break; }

		state_.MergeCoClusteredNodesBetter();

		num_merges++;
		if (verbosity_ >= Verbosity::DETAILED) { std::cout << "num nodes after merge: " << state_.graph_.NumNodes() << std::endl << "t = " << stopwatch_.TimeElapsedInSeconds() << std::endl; }
	}	

	if (verbosity_ >= Verbosity::DETAILED) { std::cout << "reached local minimum" << std::endl << "t = " << stopwatch_.TimeElapsedInSeconds() << std::endl; }

	ClusterEditingSolution solution;
	solution.cost = best_cost_;
	solution.map_node_to_cluster = best_solution_to_original_graph_;
	solution.timeout = !stopwatch_.IsWithinTimeLimit();
	return solution;
}

void ClusterEditingHeuristicSolver::PrintBestSolutionPace2021()
{
	freeze_best_solution_ = true;

	int num_edge_modifications = 0;
	//create a map cluster -> list of nodes in the cluster
	//	note that the nodes in the list of nodes are sorted by ID
	std::vector<std::vector<Node> > cluster_to_nodes(state_.graph_original_.NumNodes());
	for (int node_id = 0; node_id < state_.graph_original_.NumNodes(); node_id++)
	{
		Node node(node_id);
		Cluster cluster = best_solution_to_original_graph_[node_id];
		cluster_to_nodes[cluster.id].push_back(node);		
	}

	//for each cluster determine which edge needs to be added or removed
	//	note there may be at most as many clusters as there are nodes
	for (int cluster_id = 0; cluster_id < state_.graph_original_.NumNodes(); cluster_id++)
	{
		std::vector<Node>& cluster_nodes = cluster_to_nodes[cluster_id];
		for (Node node : cluster_nodes)
		{
			state_.graph_original_.SortNeighboursByID(node);
			const std::vector<PairNodeEdgeWeight>& neighbours = state_.graph_original_.GetAdjacencyListForNode(node);
			//the goal is to determine which neighbours are 1) not in the cluster of the node and 2) are in the cluster but not neighbours of the node
			//	for the former we remove edges, whereas for the latter we add edges
			//we have two vectors of nodes (neighbours of the node and the nodes in the cluster), each sorted by ID
			//so we can determine both of these by a linear scan through both arrays
			int i = 0, j = 0;
			while (i < neighbours.size() && j < cluster_nodes.size())
			{
				//neighbour is not in the cluster, modify edge
				if (neighbours[i].node.id < cluster_nodes[j].id) 
				{
					//each edge is visited twice in this main loop
					//here we ensure it is printed only once
					if (node.id < neighbours[i].node.id)
					{
						printf("%d %d\n", node.id+1, neighbours[i].node.id+1);
						num_edge_modifications++;
					}
					i++;
				}
				//one of the cluster nodes is not a neighbour of the node
				else if (neighbours[i].node.id > cluster_nodes[j].id)
				{					
					//as in the previous case, each edge is visited twice, but we ensure we only print it once
					if (node.id < cluster_nodes[j].id)
					{
						printf("%d %d\n", node.id+1, cluster_nodes[j].id+1);
						num_edge_modifications++;
					}
					j++;
				}
				else //neighbour is in the cluster, no modification necessary
				{
					i++;
					j++;
				}
			}
			//at this point we exhausted either the neighbours or the cluster nodes (note that both is also possible)
			
			//remove edges with remaining neighbours
			while (i < neighbours.size())
			{
				//only print the edge once, as before
				if (node.id < neighbours[i].node.id)
				{
					printf("%d %d\n", node.id+1, neighbours[i].node.id+1);
					num_edge_modifications++;
				}
				i++;
			}

			//add edges to other cluster nodes
			while (j < cluster_nodes.size())
			{
				//only print the edge once, as before
				if (node.id < cluster_nodes[j].id)
				{
					printf("%d %d\n", node.id+1, cluster_nodes[j].id+1);
					num_edge_modifications++;
				}
				j++;
			}
		}
	}

	if (num_edge_modifications != best_cost_)//debugging purposes
	{
		printf("true vs reported: %d vs %d\n", num_edge_modifications, best_cost_);
		runtime_assert(num_edge_modifications == best_cost_); 
	}
	
}

void ClusterEditingHeuristicSolver::Initialise()
{
	static bool hax_already_run = false;
	runtime_assert(hax_already_run == false); //for now we only allow calling the solver once, will fix in future
	
	evaluated_clusters_.Clear();
	local_cluster_node_weights2_.Clear();
	local_edge_weights2_.Clear();

	if (evaluated_clusters_.GetCapacity() < state_.graph_.NumNodes())
	{
		evaluated_clusters_.Resize(state_.graph_.NumNodes());
		local_cluster_node_weights2_.Resize(state_.graph_.NumNodes());
		local_edge_weights2_.Resize(state_.graph_.NumNodes());
	}	

	state_.Initialise();

	UpdateBestSolution();
}

void ClusterEditingHeuristicSolver::UpdateBestSolution()
{
	if (freeze_best_solution_) { return; }

	if (best_cost_ > state_.current_cost_)
	{
		if (best_solution_to_original_graph_.empty())
		{
			best_solution_to_original_graph_.resize(state_.graph_original_.NumNodes(), Cluster(-1));
			for (int node_id = 0; node_id < state_.graph_original_.NumNodes(); node_id++)
			{
				best_solution_to_original_graph_[node_id] = Cluster(node_id);
			}
		}

		for (int node_id = 0; node_id < state_.original_node_to_current_node_.size(); node_id++)
		{
			Cluster c = state_.original_node_to_current_node_[node_id];
			Cluster new_c = state_.GetClusterAssignmentForNode(Node(c.id));
			best_solution_to_original_graph_[node_id] = new_c;			
		}

		best_cost_ = state_.current_cost_;

		if (verbosity_ >= Verbosity::LIMITED) { std::cout << "cost " << best_cost_ << std::endl << "t = " << stopwatch_.TimeElapsedInSeconds() << std::endl; }
	}
}

void ClusterEditingHeuristicSolver::RandomiseCurrentSolution()
{
	for (int node_id = 0; node_id < state_.graph_.NumNodes(); node_id++)
	{
		Node node(node_id);
		const auto& neighbours = state_.graph_.GetAdjacencyListForNode(node);
		int neighbor_index = random_number_generator_() % (neighbours.size() + 1);

		if (neighbor_index == neighbours.size()) { continue; } //stay in current cluster

		Cluster neighbour_cluster = state_.GetClusterAssignmentForNode(neighbours[neighbor_index].node);
		state_.AssignClusterToNode(node, neighbour_cluster);
	}
}

Cluster ClusterEditingHeuristicSolver::ComputeBestLocalClusterAssignmentForNodeBetter(Node node)
{
	runtime_assert(local_cluster_node_weights2_.IsEmpty());
	runtime_assert(local_edge_weights2_.IsEmpty());

	Cluster cluster_current = state_.GetClusterAssignmentForNode(node);
	int node_weight = state_.graph_.GetNodeWeight(node);

	for (const PairNodeEdgeWeight& neighbour : state_.graph_.GetAdjacencyListForNode(node))
	{
		Cluster neighbouring_cluster = state_.GetClusterAssignmentForNode(neighbour.node);
		int nw = state_.graph_.GetNodeWeight(neighbour.node);
		//local_weights_.IncreaseWeights(neighbouring_cluster, node_weight, neighbour.edge_weight);
		local_cluster_node_weights2_[neighbouring_cluster.id] += nw;
		local_edge_weights2_[neighbouring_cluster.id] += neighbour.edge_weight;		
	}

	if (state_.empty_clusters_.GetNumPresentValues() > 0 && state_.GetNodeWeightsForCluster(cluster_current) > node_weight)
	{
		Cluster empty_cluster(*state_.empty_clusters_.begin());
		//.IncreaseWeights(empty_cluster, 0, 0);//this adds the empty_cluster_id as a key to the data structure
		local_cluster_node_weights2_[empty_cluster.id] = 0;
		local_edge_weights2_[empty_cluster.id] = 0;
	}

	int cost_of_leaving =
		-(node_weight * (state_.GetNodeWeightsForCluster(cluster_current) - node_weight))
		+ local_edge_weights2_[cluster_current.id]// current_cluster_weights.edge_weight
		+(node_weight * local_cluster_node_weights2_[cluster_current.id]); // current_cluster_weights.node_weight);

	int best_objective_improvement = 0;
	Cluster best_cluster = state_.GetClusterAssignmentForNode(node);
	int num_identical_solutions = 1;
 	for (int cluster_candidate_id : local_edge_weights2_.GetKeys())//local_weights_.GetClusters())
	{
		Cluster cluster_candidate(cluster_candidate_id);

		if (cluster_candidate == cluster_current) { continue; }

		int cost_of_entering =
			(node_weight * state_.GetNodeWeightsForCluster(cluster_candidate))
			- local_edge_weights2_[cluster_candidate.id]//  candidate_weights.edge_weight
			- (node_weight * local_cluster_node_weights2_[cluster_candidate.id]);//candidate_weights.node_weight);

		int current_improvement = (cost_of_leaving + cost_of_entering);
		num_identical_solutions += (current_improvement == best_objective_improvement);
		if (current_improvement < best_objective_improvement
			|| 
			(current_improvement == best_objective_improvement && ((random_number_generator_() % 100) <= (100/num_identical_solutions)))
			)
		{
			best_objective_improvement = current_improvement;
			best_cluster = cluster_candidate;
			num_identical_solutions = 1;
		}
	}
	runtime_assert(best_cluster != cluster_current || best_objective_improvement == 0); //todo remove this debug

	local_edge_weights2_.Clear();
	local_cluster_node_weights2_.Clear();

	return best_cluster;
}

Cluster ClusterEditingHeuristicSolver::ComputeBestLocalClusterAssignmentForNode(Node node)
{
	runtime_assert(local_cluster_node_weights_.empty());
	runtime_assert(local_edge_weights_.empty());

	Cluster cluster_current = state_.GetClusterAssignmentForNode(node);
	int node_weight = state_.graph_.GetNodeWeight(node);

	for (const PairNodeEdgeWeight& neighbour : state_.graph_.GetAdjacencyListForNode(node))
	{
		Cluster neighbouring_cluster = state_.GetClusterAssignmentForNode(neighbour.node);
		local_cluster_node_weights_[neighbouring_cluster.id] += state_.graph_.GetNodeWeight(neighbour.node);
		local_edge_weights_[neighbouring_cluster.id] += neighbour.edge_weight;
	}

	if (state_.empty_clusters_.GetNumPresentValues() > 0)
	{
		Cluster empty_cluster(*state_.empty_clusters_.begin());
		local_edge_weights_[empty_cluster.id] = 0;
		local_cluster_node_weights_[empty_cluster.id] = 0;
	}

	//pesimistically/optimistically assume that the each node in the new/old cluster was disconnected, adjust the cost accordingly
	//in the next loop the errors introduced by these assumptions will be fixed

	int cost_of_leaving = 
		- (node_weight * (state_.GetNodeWeightsForCluster(cluster_current) - node_weight)) 
		+ local_edge_weights_[cluster_current.id] 
		+ (node_weight*local_cluster_node_weights_[cluster_current.id]);

	int best_objective_improvement = 0;
	Cluster best_cluster = state_.GetClusterAssignmentForNode(node);
	for (auto& pair_cluster_edge : local_edge_weights_)
	{
		Cluster cluster_candidate(pair_cluster_edge.first);

		if (cluster_candidate == cluster_current) { continue; }

		int edge_weights = pair_cluster_edge.second;
		int node_weights = local_cluster_node_weights_[cluster_candidate.id];

		int cost_of_entering =
			(node_weight * state_.GetNodeWeightsForCluster(cluster_candidate))
			- local_edge_weights_[cluster_candidate.id]
			- (node_weight*local_cluster_node_weights_[cluster_candidate.id]);

		int current_improvement = (cost_of_leaving + cost_of_entering);
		if (current_improvement < best_objective_improvement)
		{
			best_objective_improvement = current_improvement;
			best_cluster = cluster_candidate;
		}
	}
	runtime_assert(best_cluster != cluster_current || best_objective_improvement == 0); //todo remove this debug

	local_edge_weights_.clear();
	local_cluster_node_weights_.clear();

	return best_cluster;
}

Cluster ClusterEditingHeuristicSolver::ComputeBestLocalClusterAssignmentForNodeSimple(Node node)
{
	runtime_assert(evaluated_clusters_.GetNumPresentValues() == 0);

	int best_objective_improvement = 0;
	Cluster best_cluster = state_.GetClusterAssignmentForNode(node);

	const std::vector<PairNodeEdgeWeight>& neighbours = state_.graph_.GetAdjacencyListForNode(node);
	for (const PairNodeEdgeWeight& neighbour : neighbours)
	{
		Cluster neighbouring_cluster = state_.GetClusterAssignmentForNode(neighbour.node);

		if (evaluated_clusters_.IsPresent(neighbouring_cluster.id)) { continue; }

		int current_improvement = state_.ComputeObjectiveDifferenceForAssigningClusterToNode(node, neighbouring_cluster);
		if (current_improvement > best_objective_improvement)
		{
			best_objective_improvement = current_improvement;
			best_cluster = neighbouring_cluster;
		}

		evaluated_clusters_.Insert(neighbouring_cluster.id);
	}

	//also consider the case when the node is moved to its own isolated cluster
	if (state_.empty_clusters_.GetNumPresentValues() > 0)
	{
		Cluster empty_cluster = Cluster(*state_.empty_clusters_.begin());
		int current_improvement = state_.ComputeObjectiveDifferenceForAssigningClusterToNode(node, empty_cluster);
		if (current_improvement > best_objective_improvement)
		{
			best_objective_improvement = current_improvement;
			best_cluster = empty_cluster;
		}
	}

	evaluated_clusters_.Clear();
	return best_cluster;
}