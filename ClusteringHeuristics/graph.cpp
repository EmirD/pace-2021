#include "graph.h"
#include "runtime_assert.h"

#include <iostream>
#include <fstream>
#include <algorithm>

Graph::Graph()
{
}

Graph::Graph(std::string file_location, bool read_from_standard_input)
{
	//the current version assumes comments are not present...todo fix

	if (read_from_standard_input)
	{
		char c;
		std::string s;
		std::cin >> c;
		runtime_assert(c == 'p');
		std::cin >> s;
		runtime_assert(s == "cep");
		int num_nodes, num_edges;
		std::cin >> num_nodes >> num_edges;

		struct Edge { Node node1, node2; Edge(int node1_id, int node2_id) :node1(node1_id), node2(node2_id) {}; };
		std::vector<Edge> edges(num_edges, Edge(-1, -1));
		std::vector<int> num_neighbours(num_nodes, 0);
		int node1_id, node2_id;
		for (int i = 0; i < num_edges; i++)
		{
			std::cin >> node1_id >> node2_id;
			edges[i].node1 = Node(node1_id - 1);
			edges[i].node2 = Node(node2_id - 1);
			num_neighbours[node1_id - 1]++;
			num_neighbours[node2_id - 1]++;
		}
		
		node_weights_.resize(num_nodes, 1);
		adjacency_lists_.resize(num_nodes);
		for (int node_id = 0; node_id < num_nodes; node_id++) { adjacency_lists_[node_id].reserve(num_neighbours[node_id]); }

		for (int i = 0; i < num_edges; i++)
		{
			adjacency_lists_[edges[i].node1.id].push_back(PairNodeEdgeWeight(edges[i].node2, 1));
			adjacency_lists_[edges[i].node2.id].push_back(PairNodeEdgeWeight(edges[i].node1, 1));
		}

		return;
	}
	else
	{
		std::ifstream input_file(file_location.c_str());
		char c;
		std::string s;
		input_file >> c;
		runtime_assert(c == 'p');
		input_file >> s;
		runtime_assert(s == "cep");
		int num_nodes, num_edges;
		input_file >> num_nodes >> num_edges;

		struct Edge { Node node1, node2; Edge(int node1_id, int node2_id) :node1(node1_id), node2(node2_id) {}; };
		std::vector<Edge> edges(num_edges, Edge(-1, -1));
		std::vector<int> num_neighbours(num_nodes, 0);
		int node1_id, node2_id;
		for (int i = 0; i < num_edges; i++)
		{
			input_file >> node1_id >> node2_id;
			edges[i].node1 = Node(node1_id - 1);
			edges[i].node2 = Node(node2_id - 1);
			num_neighbours[node1_id - 1]++;
			num_neighbours[node2_id - 1]++;
		}
		input_file >> s;
		runtime_assert(!input_file);

		node_weights_.resize(num_nodes, 1);
		adjacency_lists_.resize(num_nodes);
		for (int node_id = 0; node_id < num_nodes; node_id++) { adjacency_lists_[node_id].reserve(num_neighbours[node_id]); }

		for (int i = 0; i < num_edges; i++)
		{
			adjacency_lists_[edges[i].node1.id].push_back(PairNodeEdgeWeight(edges[i].node2, 1));
			adjacency_lists_[edges[i].node2.id].push_back(PairNodeEdgeWeight(edges[i].node1, 1));
		}

		return;

		node_weights_.resize(num_nodes, 1);
		adjacency_lists_.resize(num_nodes);
		for (int i = 0; i < num_edges; i++)
		{
			int node1, node2;
			input_file >> node1 >> node2;
			adjacency_lists_[node1 - 1].push_back(PairNodeEdgeWeight(Node(node2 - 1), 1));
			adjacency_lists_[node2 - 1].push_back(PairNodeEdgeWeight(Node(node1 - 1), 1));
		}
		input_file >> s;
		runtime_assert(!input_file);
	}
}

Graph::Graph(const std::vector<std::vector<PairNodeEdgeWeight>>& adjacency_lists, const std::vector<int>& node_weights):
	adjacency_lists_(adjacency_lists),
	node_weights_(node_weights)
{

}

bool Graph::operator==(Graph& rhs)
{
	for (int node1_id = 0; node1_id < NumNodes(); node1_id++)
	{
		Node node1(node1_id);

		if (GetAdjacencyListForNode(node1).size() != rhs.GetAdjacencyListForNode(node1).size())
		{
			return false;
		}

		for (auto& neighbour : GetAdjacencyListForNode(node1))
		{
			if (neighbour.edge_weight != rhs.GetEdgeCost(node1, neighbour.node))
			{
				return false;
			}
		}
	}

	for (int i = 0; i < node_weights_.size(); i++)
	{
		if (node_weights_[i] != rhs.node_weights_[i])
		{
			return false;
		}
	}
	return true;
}

int Graph::NumNodes() const
{
	return adjacency_lists_.size();
}

int64_t Graph::NumEdges() const
{
	int64_t num_edges = 0;
	for (const auto& v : adjacency_lists_) { num_edges += v.size(); }
	return num_edges/2; //each edge is counted twice, so we divide by two
}

int Graph::GetNodeWeight(Node node) const
{
	return node_weights_[node.id];
}

void Graph::SortNeighboursByID(Node node)
{
	std::sort
	(
		adjacency_lists_[node.id].begin(), 
		adjacency_lists_[node.id].end(), 
		[](const PairNodeEdgeWeight& n1, const PairNodeEdgeWeight& n2)->bool { return n1.node.id < n2.node.id; }
	);
}

const std::vector<PairNodeEdgeWeight>& Graph::GetAdjacencyListForNode(Node node)
{
	return adjacency_lists_[node.id];
}

bool Graph::AreNeighbours(Node node1, Node node2) const
{
	if (adjacency_lists_[node1.id].size() > adjacency_lists_[node2.id].size()) { std::swap(node1, node2); }

	for (const PairNodeEdgeWeight neighbour : adjacency_lists_[node1.id])
	{
		if (neighbour.node == node2) { return true; }
	}
	return false;
}

int Graph::GetEdgeCost(Node node1, Node node2) const
{
	if (adjacency_lists_[node1.id].size() > adjacency_lists_[node2.id].size()) { std::swap(node1, node2); }

	for (const PairNodeEdgeWeight neighbour : adjacency_lists_[node1.id])
	{
		if (neighbour.node == node2) { return neighbour.edge_weight; }
	}
	runtime_assert(1 == 2); //should never reach here	
}
