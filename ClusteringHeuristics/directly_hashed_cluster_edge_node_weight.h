#pragma once

#include "cluster.h"

#include <vector>

//todo description
//as c++ set, assumes that everything has value zero by default
//not entirely safe, uses NOT_PRESENT to denote that the value is not present, but the value might be equal to NOT_PRESENT
//	nevertheless okay for this project
class DirectlyHashedClusterEdgeNodeWeight
{
struct EdgeNodeWeight { int edge_weight, node_weight; EdgeNodeWeight(int ew, int nw) :edge_weight(ew), node_weight(nw) {}; };

public:	
	DirectlyHashedClusterEdgeNodeWeight(int num_clusters); //the keys must be in the set [0,.., size).

	EdgeNodeWeight& operator[](Cluster cluster);
	void IncreaseWeights(Cluster cluster, int node_weight, int edge_weight);

	void Resize(int new_size); //assumes the data structure is empty when sizing
	bool HasCluster(Cluster cluster) const;
	void Clear();

	int GetNumClustersPresent() const;
	bool IsEmpty() const;
	const std::vector<Cluster>& GetClusters() const; //potentially dangerous, should not modify the data structure when iterating over it

private:
	static const int NOT_PRESENT;

	std::vector<Cluster> present_clusters_;
	std::vector<EdgeNodeWeight> cluster_id_to_values_;//[i] are the values for the i-th cluster
};