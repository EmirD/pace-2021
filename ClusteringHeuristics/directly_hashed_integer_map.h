#pragma once

#include <vector>

//todo description
//as c++ set, assumes that everything has value zero by default
//not entirely safe, uses NOT_PRESENT to denote that the value is not present, but the value might be equal to NOT_PRESENT
//	nevertheless okay for this project
class DirectlyHashedIntegerMap
{
public:
	DirectlyHashedIntegerMap(int size); //the keys must be in the set [0,.., size).

	int& operator[](int key);

	void Resize(int new_size); //assumes the data structure is empty when sizing
	bool HasKey(int key) const;
	void Clear();	
	
	int GetNumKeysPresent() const;	
	bool IsEmpty() const;
	const std::vector<int>& GetKeys() const; //potentially dangerous, should not modify the data structure when iterating over it

private:
	static const int NOT_PRESENT;

	std::vector<int> present_keys_;
	std::vector<int> keys_to_values_;//[i] is the value for the i-th key
};