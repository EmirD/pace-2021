#include "cluster_editing_heuristic_solver.h"
#include "verifier/verifier.h"
#include "runtime_assert.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <signal.h>

ClusterEditingHeuristicSolver* g_solver = NULL;

static void SIGTERM_PrintSolutionAndTerminate(int)
{
	if (g_solver == NULL) { std::cout << "Error: The solver is NULL?\n...probably using the Pace2021 flag without providing the instance through the standard input, or in extreme cases, solver aborted before instance file was fully read (takes at most ten seconds to read the largest files in my experience)\n"; }

	std::cout.setstate(std::ios_base::failbit); //to prevent the solver from possibly printing things while the solution is being printed, could be done better
	g_solver->PrintBestSolutionPace2021();
	exit(0);
}

int main(int argc, char* argv[])
{
	signal(SIGTERM, SIGTERM_PrintSolutionAndTerminate);

	ParameterHandler parameters = ClusterEditingHeuristicSolver::CreateParameterHandler();
	//parameters.ParseCommandLineArguments(argc, argv); //uncomment this to read command line parameters
	//parameters.SetStringParameter("file", file_location);
	//parameters.SetIntegerParameter("time", 60);
	parameters.SetBooleanParameter("PACE2021-input", true); //set to false to read from files rather than standard input
	parameters.SetIntegerParameter("seed", 1);
	parameters.SetStringParameter("verbosity", "PACE2021"); //PACE2021 //limited //detailed

	ClusterEditingHeuristicSolver solver(parameters);
	g_solver = &solver;
	while (1 == 1)
	{
		ClusterEditingSolution solution = solver.Solve();
		if (solution.timeout) { break; }
	}
	SIGTERM_PrintSolutionAndTerminate(SIGTERM);
	return 0;
}
