#include <fstream>
#include <iostream>
#include <optional>
#include <sstream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>
#include "Graph.h"

//this is a modification of the original verifier provided by the Pace Challenge 2021

namespace akt
{
class Verifier
{	
public:
	static int verify(const std::string &graph_file, const std::string &solution_file)
	{
		Graph orig;
		Graph::EdgeSet sol;
		std::ifstream ifsol{ solution_file };
		if (ifsol) {
			std::string line;
			while (std::getline(ifsol, line)) {
				auto iss = std::istringstream(line);
				int u, v;
				if (!(iss >> u >> v)) {
					std::cerr << "An invalid line or not enough lines in the input. Offending line:\n" + line + "\n" << std::endl;
				}
				--u; --v;
				sol.insert(Graph::Edge(u, v));
			}
			std::ifstream ifs{ graph_file };
			if (ifs) {
				try {
					orig = Graph(ifs);
					orig.symmetricDifference(sol);
					auto p3 = orig.findP3();
					if (std::get<0>(p3) == -1) std::cout << "OK" << std::endl;
					else std::cout << "P3:" << std::get<0>(p3) + 1 << " " << std::get<1>(p3) + 1 << " " << std::get<2>(p3) + 1 << std::endl;
				}
				catch (const std::invalid_argument& ia) {
					return -1;
				}
			}
			else { return -1; }
		}
		return 0;
	}
};
}