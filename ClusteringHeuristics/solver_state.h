#pragma once

#include "graph.h"
#include "cluster.h"
#include "directly_hashed_integer_set.h"

#include <string>

class SolverState
{
public:
	//the file is a graph file in the Pace Challenge format
	//todo description
	SolverState(std::string file_location, bool read_from_standard_input);

	void Initialise();

	Cluster GetClusterAssignmentForNode(Node node) const;
	int GetNodeWeightsForCluster(Cluster cluster) const;
	bool IsClusterEmpty(Cluster cluster) const;

	int ComputeObjectiveDifferenceForAssigningClusterToNode(Node node, Cluster cluster); //a positive value indicates the objective would decrease by the returned amount (beneficial move), whereas a negative value is the opposite (harmful move)

	void IncreaseNodeWeightsForCluster(Cluster cluster, int amount);
	void AssignClusterToNode(Node node, Cluster cluster);

	void MergeCoClusteredNodesBetter();
	void MergeCoClusteredNodes();
	void MergeCoClusteredNodesSimple();

	static int ComputeCostFromScratchForGraph(const Graph graph, const std::vector<Cluster> &node_to_cluster);

	Graph graph_;
	int64_t current_cost_;
	int64_t constant_term_;

//private:
	Graph graph_original_;
	std::vector<Cluster> original_node_to_current_node_; //during the algorithm nodes are merged into larger nodes. This vector maps the original node to its node in the currently considered graph.

	DirectlyHashedIntegerSet empty_clusters_; //keeps track of which cluster_ids do not contain any nodes
	std::vector<int> node_weights_for_cluster_; //[i] is the sum of all node weights for cluster i
	std::vector<Cluster> node_to_cluster_; //[node_id] is the cluster to which node_id is assigned to
};