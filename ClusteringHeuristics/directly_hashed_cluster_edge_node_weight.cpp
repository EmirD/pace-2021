#include "directly_hashed_cluster_edge_node_weight.h"
#include "runtime_assert.h"

#include <stdint.h>

const int DirectlyHashedClusterEdgeNodeWeight::NOT_PRESENT = INT32_MIN;

DirectlyHashedClusterEdgeNodeWeight::DirectlyHashedClusterEdgeNodeWeight(int num_clusters):
	present_clusters_(),
	cluster_id_to_values_(num_clusters, EdgeNodeWeight(NOT_PRESENT, NOT_PRESENT))
{
}

DirectlyHashedClusterEdgeNodeWeight::EdgeNodeWeight& DirectlyHashedClusterEdgeNodeWeight::operator[](Cluster cluster)
{
	if (!HasCluster(cluster))
	{
		cluster_id_to_values_[cluster.id].edge_weight = 0;
		cluster_id_to_values_[cluster.id].node_weight = 0;
		present_clusters_.push_back(cluster);
	}
	return cluster_id_to_values_[cluster.id];
}

/*void DirectlyHashedClusterEdgeNodeWeight::IncreaseWeights(Cluster cluster, int node_weight, int edge_weight)
{
	if (!HasCluster(cluster))
	{
		cluster_id_to_values_[cluster.id].edge_weight = 0;
		cluster_id_to_values_[cluster.id].node_weight = 0;
		present_clusters_.push_back(cluster);
	}
	cluster_id_to_values_[cluster.id].edge_weight += edge_weight;
	cluster_id_to_values_[cluster.id].node_weight += node_weight;
}*/

void DirectlyHashedClusterEdgeNodeWeight::Resize(int new_size)
{
	runtime_assert(GetNumClustersPresent() == 0);
	cluster_id_to_values_.resize(new_size, EdgeNodeWeight(NOT_PRESENT, NOT_PRESENT));
}

bool DirectlyHashedClusterEdgeNodeWeight::HasCluster(Cluster cluster) const
{
	return cluster_id_to_values_[cluster.id].edge_weight != NOT_PRESENT;
}

void DirectlyHashedClusterEdgeNodeWeight::Clear()
{
	for (Cluster cluster : present_clusters_) { cluster_id_to_values_[cluster.id].edge_weight = NOT_PRESENT; }
	present_clusters_.clear();
}

int DirectlyHashedClusterEdgeNodeWeight::GetNumClustersPresent() const
{
	return int(present_clusters_.size());
}

bool DirectlyHashedClusterEdgeNodeWeight::IsEmpty() const
{
	return present_clusters_.empty();
}

const std::vector<Cluster>& DirectlyHashedClusterEdgeNodeWeight::GetClusters() const
{
	return present_clusters_;
}
