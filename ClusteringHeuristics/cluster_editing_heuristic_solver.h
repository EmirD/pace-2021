#pragma once

#include "solver_state.h"
#include "directly_hashed_integer_set.h"
#include "directly_hashed_integer_map.h"
#include "directly_hashed_cluster_edge_node_weight.h"
#include "stopwatch.h"
#include "parameter_handler.h"

#include <vector>
#include <map>
#include <string>
#include <random>
#include <iostream>

struct ClusterEditingSolution
{
	std::vector<Cluster> map_node_to_cluster;
	int cost;
	bool timeout;
};

class ClusterEditingHeuristicSolver
{
public:
	ClusterEditingHeuristicSolver(ParameterHandler &parameters);

	static ParameterHandler CreateParameterHandler();

	ClusterEditingSolution Solve();
	void PrintBestSolutionPace2021();

//private:

	void Initialise();
	void UpdateBestSolution();
	void RandomiseCurrentSolution();
	Cluster ComputeBestLocalClusterAssignmentForNodeBetter(Node node);
	Cluster ComputeBestLocalClusterAssignmentForNode(Node node);
	Cluster ComputeBestLocalClusterAssignmentForNodeSimple(Node node);
	
	enum class Verbosity { PACE2021, LIMITED, DETAILED } verbosity_;
	
	Stopwatch stopwatch_;
	DirectlyHashedIntegerSet evaluated_clusters_;
	DirectlyHashedIntegerMap local_cluster_node_weights2_;
	DirectlyHashedIntegerMap local_edge_weights2_;
	std::map<int, int> local_cluster_node_weights_;
	std::map<int, int> local_edge_weights_;
	std::vector<Cluster> best_solution_to_original_graph_;
	std::mt19937 random_number_generator_;
	int64_t best_cost_;
	int64_t num_iterations_total_;
	int seed_;
	bool randomise_sequence_;
	bool freeze_best_solution_; //used in the Pace2021 competition. After a stop signal is received, this is set to true, which instructs the solver to not update the best solution while it is being printed.
	SolverState state_;
};