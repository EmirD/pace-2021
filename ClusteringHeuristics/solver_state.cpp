#include "solver_state.h"
#include "directly_hashed_integer_map.h"
#include "runtime_assert.h"

#include <iostream>
#include <map>

SolverState::SolverState(std::string file_location, bool read_from_standard_input) :
	graph_original_(file_location, read_from_standard_input),
	empty_clusters_(0)
{
	Initialise();
}

void SolverState::Initialise()
{
	graph_ = graph_original_;
	node_weights_for_cluster_ = std::vector<int>(graph_.NumNodes(), 1);
	empty_clusters_.Clear();
	if (empty_clusters_.GetCapacity() < graph_.NumNodes()) { empty_clusters_.Resize(graph_.NumNodes()); }
	//initially each node is in its own cluster
	node_to_cluster_.clear();
	for (int node_id = 0; node_id < graph_.NumNodes(); node_id++) { node_to_cluster_.push_back(Cluster(node_id)); }
	original_node_to_current_node_ = node_to_cluster_;
	current_cost_ = graph_.NumEdges();
	constant_term_ = 0;			
}

Cluster SolverState::GetClusterAssignmentForNode(Node node) const
{
	return node_to_cluster_[node.id];
}

int SolverState::GetNodeWeightsForCluster(Cluster cluster) const
{
	return node_weights_for_cluster_[cluster.id];
}

bool SolverState::IsClusterEmpty(Cluster cluster) const
{
	return node_weights_for_cluster_[cluster.id] == 0;
}

void SolverState::IncreaseNodeWeightsForCluster(Cluster cluster, int amount)
{
	node_weights_for_cluster_[cluster.id] += amount;
	runtime_assert(node_weights_for_cluster_[cluster.id] >= 0);
}

int SolverState::ComputeObjectiveDifferenceForAssigningClusterToNode(Node node, Cluster cluster)
{
	int64_t old_objective = current_cost_;
	Cluster initial_cluster = GetClusterAssignmentForNode(node);
	AssignClusterToNode(node, cluster);
	int64_t difference = old_objective - current_cost_;
	AssignClusterToNode(node, initial_cluster);

	runtime_assert(current_cost_ == old_objective);
	return difference;
}

void SolverState::AssignClusterToNode(Node node, Cluster cluster_new)
{
	if (GetClusterAssignmentForNode(node) == cluster_new) { return; }

	Cluster cluster_old = GetClusterAssignmentForNode(node);
	int node_weight = graph_.GetNodeWeight(node);

	//pesimistically/optimistically assume that the each node in the new/old cluster was disconnected, adjust the cost accordingly
	//in the next loop the errors introduced by these assumptions will be fixed
	current_cost_ += (node_weight * GetNodeWeightsForCluster(cluster_new));
	current_cost_ -= (node_weight * (GetNodeWeightsForCluster(cluster_old) - node_weight));

	const std::vector<PairNodeEdgeWeight>& neighbours = graph_.GetAdjacencyListForNode(node);
	for (const PairNodeEdgeWeight& neighbour : neighbours)
	{
		Cluster neighbouring_cluster = GetClusterAssignmentForNode(neighbour.node);
		
		if (neighbouring_cluster == cluster_old)
		{
			current_cost_ += neighbour.edge_weight;
			current_cost_ += (node_weight * graph_.GetNodeWeight(neighbour.node));
		}
		else if (neighbouring_cluster == cluster_new)
		{
			current_cost_ -= neighbour.edge_weight;
			current_cost_ -= (node_weight * graph_.GetNodeWeight(neighbour.node));
		}
	}	

	//if the new cluster was empty before, it will not be empty after the move, so we remove it from the list of empty clusters
	if (IsClusterEmpty(cluster_new)) { empty_clusters_.Remove(cluster_new.id); }
	
	IncreaseNodeWeightsForCluster(cluster_old, -node_weight);
	IncreaseNodeWeightsForCluster(cluster_new, node_weight);
	node_to_cluster_[node.id] = cluster_new;	

	//the old cluster might have become empty after removing the node, so we should record it
	if (IsClusterEmpty(cluster_old)) { empty_clusters_.Insert(cluster_old.id); }

	/*int s = ComputeCostFromScratchForGraph(graph_, node_to_cluster_);
	runtime_assert(current_cost_ == s + constant_term_);
	int m = 0;*/
}

void SolverState::MergeCoClusteredNodesBetter()
{
	{
		//rename clusters to so that their ids are in the range [0, num_clusters)
		int num_clusters = 0;
		std::vector<Cluster> cluster_to_new_cluster(graph_.NumNodes(), Cluster(-1));
		for (int node_id = 0; node_id < graph_.NumNodes(); node_id++)
		{
			Cluster cluster = GetClusterAssignmentForNode(Node(node_id));
			if (cluster_to_new_cluster[cluster.id] == Cluster(-1))
			{
				cluster_to_new_cluster[cluster.id] = Cluster(num_clusters++);
			}
		}
		std::vector<int> new_node_weights_for_cluster(num_clusters, 0);
		for (int old_cluster_id = 0; old_cluster_id < graph_.NumNodes(); old_cluster_id++)
		{
			if (cluster_to_new_cluster[old_cluster_id] == Cluster(-1)) { continue; }

			int new_cluster_id = cluster_to_new_cluster[old_cluster_id].id;
			new_node_weights_for_cluster[new_cluster_id] = node_weights_for_cluster_[old_cluster_id];
		}

		node_weights_for_cluster_ = new_node_weights_for_cluster;

		for (int node_id = 0; node_id < graph_.NumNodes(); node_id++)
		{
			Cluster old_cluster = node_to_cluster_[node_id];
			node_to_cluster_[node_id] = cluster_to_new_cluster[old_cluster.id];
		}
		//-----end renaming clusters

		//update the solution to the original instance
		for (int node_id = 0; node_id < original_node_to_current_node_.size(); node_id++)
		{
			Cluster c = original_node_to_current_node_[node_id];
			Cluster c2 = node_to_cluster_[c.id];
			original_node_to_current_node_[node_id] = c2;
		}

		std::vector<std::vector<Node> > cluster_to_nodes(num_clusters);
		std::vector<int> cluster_node_weights(num_clusters, 0);
		for (int node_id = 0; node_id < graph_.NumNodes(); node_id++)
		{
			Node node(node_id);
			Cluster cluster(GetClusterAssignmentForNode(node));
			cluster_to_nodes[cluster.id].push_back(node);
			cluster_node_weights[cluster.id] += graph_.GetNodeWeight(node);
		}
		
		std::vector<std::vector<PairNodeEdgeWeight> > new_adjacency_lists(num_clusters);
		DirectlyHashedIntegerMap cluster_edges(num_clusters);
		for (int cluster_id = 0; cluster_id < num_clusters; cluster_id++)
		{
			cluster_edges.Clear();
			for (Node node : cluster_to_nodes[cluster_id])
			{				
				for (const PairNodeEdgeWeight& neighbour : graph_.GetAdjacencyListForNode(node))
				{
					Cluster neighbouring_cluster = GetClusterAssignmentForNode(neighbour.node);
					cluster_edges[neighbouring_cluster.id] += neighbour.edge_weight;					
				}				
			}

			new_adjacency_lists[cluster_id].reserve(cluster_edges.GetNumKeysPresent());
			for (int neighbour_cluster_id : cluster_edges.GetKeys())
			{
				Node cluster_as_node(neighbour_cluster_id);

				if (cluster_id == neighbour_cluster_id) { continue; }

				new_adjacency_lists[cluster_id].push_back(PairNodeEdgeWeight(cluster_as_node, cluster_edges[neighbour_cluster_id]));
			}
		}

		DirectlyHashedIntegerMap cluster_node_weights_for_node(num_clusters);
		for (int cluster_id = 0; cluster_id < num_clusters; cluster_id++)
		{			
			for (Node node : cluster_to_nodes[cluster_id])
			{
				cluster_node_weights_for_node.Clear();
				for (const PairNodeEdgeWeight& neighbour : graph_.GetAdjacencyListForNode(node))
				{
					Cluster neighbouring_cluster = GetClusterAssignmentForNode(neighbour.node);
					cluster_node_weights_for_node[neighbouring_cluster.id] += graph_.GetNodeWeight(neighbour.node);
				}

				for (PairNodeEdgeWeight &neighbour_cluster : new_adjacency_lists[cluster_id])
				{
					int neighbour_id = neighbour_cluster.node.id;
					int nonconnected_node_weights = cluster_node_weights[neighbour_id];
					if (cluster_node_weights_for_node.HasKey(neighbour_id))
					{
						nonconnected_node_weights -= cluster_node_weights_for_node[neighbour_id];
					}
					neighbour_cluster.edge_weight -= (graph_.GetNodeWeight(node) * nonconnected_node_weights);
					int m = 0;
				}
			}
		}

		graph_ = Graph(new_adjacency_lists, cluster_node_weights);

		node_to_cluster_.clear();
		node_weights_for_cluster_.clear();
		for (int cluster_id = 0; cluster_id < num_clusters; cluster_id++)
		{
			node_to_cluster_.push_back(Cluster(cluster_id));
			node_weights_for_cluster_.push_back(cluster_node_weights[cluster_id]);
		}

		int initial_graph_cost = 0;
		for (int node_id = 0; node_id < graph_.NumNodes(); node_id++)
		{
			Node node(node_id);
			for (const PairNodeEdgeWeight& neighbour : graph_.GetAdjacencyListForNode(node))
			{
				initial_graph_cost += neighbour.edge_weight;
			}
		}
		initial_graph_cost /= 2;

		constant_term_ = current_cost_ - initial_graph_cost;

		empty_clusters_.Clear();
		return;
	}

	

	std::vector<std::map<int, int> > cluster_to_cluster_edge_interaction(graph_.NumNodes());

	std::vector<std::vector<Node> > cluster_to_nodes(graph_.NumNodes());
	std::vector<int> cluster_node_weights(graph_.NumNodes(), 0);
	for (int node_id = 0; node_id < graph_.NumNodes(); node_id++)
	{
		Node node(node_id);
		Cluster cluster(GetClusterAssignmentForNode(node));
		cluster_to_nodes[cluster.id].push_back(node);
		cluster_node_weights[cluster.id] += graph_.GetNodeWeight(node);
	}

	//create cluster -> nodes [I already have this]
	//for cluster, can record interaction with other cluster using directly hashed data structure
	//	then covert the directly hashed into a normal hash before going to the next

	//sort nodes by cluster, then easily update? Or use directly hashed data structures
	for (int node1_id = 0; node1_id < graph_.NumNodes(); node1_id++)
	{
		Node node1(node1_id);
		Cluster cluster1(GetClusterAssignmentForNode(node1));
		//seen_cluster.Clear();
		for (const PairNodeEdgeWeight& neighbour : graph_.GetAdjacencyListForNode(node1)) //todo: could consider sorting the nodes according to their cluster to reduce the map find operations, but probably does not make a big difference
		{
			Cluster cluster2(GetClusterAssignmentForNode(neighbour.node));

			if (cluster1 == cluster2) { continue; }
			
			cluster_to_cluster_edge_interaction[cluster1.id][cluster2.id] = 0; //HAX todo
			cluster_to_cluster_edge_interaction[cluster2.id][cluster1.id] = 0; //HAX todo
			//local_weight_sum[cluster2.id] += graph_.GetNodeWeight(neighbour.node);
		}
	}

	//create cluster to nodes?
	DirectlyHashedIntegerMap local_weight_sum(graph_.NumNodes());
	for (int node1_id = 0; node1_id < graph_.NumNodes(); node1_id++)
	{
		Node node1(node1_id);
		Cluster cluster1(GetClusterAssignmentForNode(node1));
		local_weight_sum.Clear();
		for (const PairNodeEdgeWeight& neighbour : graph_.GetAdjacencyListForNode(node1)) //todo: could consider sorting the nodes according to their cluster to reduce the map find operations, but probably does not make a big difference
		{
			Cluster cluster2(GetClusterAssignmentForNode(neighbour.node));

			if (cluster1 == cluster2) { continue; }

			cluster_to_cluster_edge_interaction[cluster1.id][cluster2.id] += neighbour.edge_weight;
			local_weight_sum[cluster2.id] += graph_.GetNodeWeight(neighbour.node);
		}

		for (auto& pair_cluster_value : cluster_to_cluster_edge_interaction[cluster1.id])
		{
			Cluster cluster2(pair_cluster_value.first);

			if (cluster1 == cluster2) { continue; }

			int nonconnected_node_weights = GetNodeWeightsForCluster(cluster2);
			if (local_weight_sum.HasKey(cluster2.id))
			{
				nonconnected_node_weights -= local_weight_sum[cluster2.id];
			}

			cluster_to_cluster_edge_interaction[cluster1.id][cluster2.id] -= (graph_.GetNodeWeight(node1) * nonconnected_node_weights);
		}
	}

	int num_clusters = 0;
	std::vector<Cluster> cluster_to_new_cluster(graph_.NumNodes(), Cluster(-1));
	for (int node_id = 0; node_id < graph_.NumNodes(); node_id++)
	{
		Cluster cluster = GetClusterAssignmentForNode(Node(node_id));
		if (cluster_to_new_cluster[cluster.id] == Cluster(-1))
		{
			cluster_to_new_cluster[cluster.id] = Cluster(num_clusters++);
		}
	}

	//update the solution to the original instance
	for (int node_id = 0; node_id < original_node_to_current_node_.size(); node_id++)
	{
		Cluster c = original_node_to_current_node_[node_id];
		Cluster c2 = node_to_cluster_[c.id];
		original_node_to_current_node_[node_id] = cluster_to_new_cluster[c2.id];
	}

	std::vector<std::vector<PairNodeEdgeWeight> > new_adjacency_lists(num_clusters);
	for (int cluster1_id = 0; cluster1_id < cluster_to_cluster_edge_interaction.size(); cluster1_id++)
	{
		Cluster cluster1_new = cluster_to_new_cluster[cluster1_id];
		for (auto& pair_cluster_edge : cluster_to_cluster_edge_interaction[cluster1_id])
		{
			Cluster cluster2_new = cluster_to_new_cluster[pair_cluster_edge.first];
			int weight = pair_cluster_edge.second;

			new_adjacency_lists[cluster1_new.id].push_back(PairNodeEdgeWeight(Node(cluster2_new.id), weight));
		}
	}

	std::vector<int> new_node_weights(num_clusters);
	for (int node_id = 0; node_id < graph_.NumNodes(); node_id++)
	{
		Node node(node_id);
		Cluster cluster = GetClusterAssignmentForNode(node);
		Cluster new_cluster = cluster_to_new_cluster[cluster.id];
		new_node_weights[new_cluster.id] += graph_.GetNodeWeight(node);
	}

	graph_ = Graph(new_adjacency_lists, new_node_weights);

	node_to_cluster_.clear();
	node_weights_for_cluster_.clear();
	for (int cluster_id = 0; cluster_id < num_clusters; cluster_id++)
	{
		node_to_cluster_.push_back(Cluster(cluster_id));
		node_weights_for_cluster_.push_back(new_node_weights[cluster_id]);
	}

	int initial_graph_cost = 0;
	for (int node1_id = 0; node1_id < graph_.NumNodes(); node1_id++)
	{
		Node node1(node1_id);
		Cluster cluster1(GetClusterAssignmentForNode(node1));
		for (const PairNodeEdgeWeight& neighbour : graph_.GetAdjacencyListForNode(node1))
		{
			initial_graph_cost += neighbour.edge_weight;
		}
	}
	initial_graph_cost /= 2;

	constant_term_ = current_cost_ - initial_graph_cost;

	empty_clusters_.Clear();
}

void SolverState::MergeCoClusteredNodes()
{
	//std::vector<std::vector<int> > cluster_to_cluster_edges(graph_.NumNodes(), std::vector<int>(graph_.NumNodes(), 0));
	//std::vector<std::vector<bool> > cluster_to_cluster_iteract(graph_.NumNodes(), std::vector<bool>(graph_.NumNodes(), false));

	std::vector<std::map<int, int> > cluster_to_cluster_edge_interaction(graph_.NumNodes());
	
	std::vector<std::vector<Node> > cluster_to_nodes(graph_.NumNodes());
	std::vector<int> cluster_node_weights(graph_.NumNodes(), 0);
	for (int node_id = 0; node_id < graph_.NumNodes(); node_id++)
	{
		Node node(node_id);
		Cluster cluster(GetClusterAssignmentForNode(node));
		cluster_to_nodes[cluster.id].push_back(node);
		cluster_node_weights[cluster.id] += graph_.GetNodeWeight(node);
	}

	std::map<int, int> local_weight_sum;	
	for (int node1_id = 0; node1_id < graph_.NumNodes(); node1_id++)
	{
		Node node1(node1_id);
		Cluster cluster1(GetClusterAssignmentForNode(node1));
		//local_weight_sum.clear();
		for (const PairNodeEdgeWeight &neighbour : graph_.GetAdjacencyListForNode(node1)) //todo: could consider sorting the nodes according to their cluster to reduce the map find operations, but probably does not make a big difference
		{
			Cluster cluster2(GetClusterAssignmentForNode(neighbour.node));

			if (cluster1 == cluster2) { continue; }

			cluster_to_cluster_edge_interaction[cluster1.id][cluster2.id] = 0; //HAX todo
			cluster_to_cluster_edge_interaction[cluster2.id][cluster1.id] = 0; //HAX todo
			//local_weight_sum[cluster2.id] += graph_.GetNodeWeight(neighbour.node);
		}
	}

	for (int node1_id = 0; node1_id < graph_.NumNodes(); node1_id++)
	{
		Node node1(node1_id);
		Cluster cluster1(GetClusterAssignmentForNode(node1));
		local_weight_sum.clear();
		for (const PairNodeEdgeWeight& neighbour : graph_.GetAdjacencyListForNode(node1)) //todo: could consider sorting the nodes according to their cluster to reduce the map find operations, but probably does not make a big difference
		{
			Cluster cluster2(GetClusterAssignmentForNode(neighbour.node));

			if (cluster1 == cluster2) { continue; }

			cluster_to_cluster_edge_interaction[cluster1.id][cluster2.id] += neighbour.edge_weight;
			local_weight_sum[cluster2.id] += graph_.GetNodeWeight(neighbour.node);
		}

		for (auto& pair_cluster_value : cluster_to_cluster_edge_interaction[cluster1.id])
		{
			Cluster cluster2(pair_cluster_value.first);

			if (cluster1 == cluster2) { continue; }

			int nonconnected_node_weights = GetNodeWeightsForCluster(cluster2);
			if (local_weight_sum.count(cluster2.id))
			{
				nonconnected_node_weights -= local_weight_sum[cluster2.id];
			}

			cluster_to_cluster_edge_interaction[cluster1.id][cluster2.id] -= (graph_.GetNodeWeight(node1) * nonconnected_node_weights);			
		}
	}

	//todo need to really check whether the above works even for later iterations - I think so but need to check
	/*for (int node1_id = 0; node1_id < graph_.NumNodes(); node1_id++)
	{
		Node node1(node1_id);
		Cluster cluster1(GetClusterAssignmentForNode(node1));
		for (int node2_id = 0; node2_id < graph_.NumNodes(); node2_id++)
		{
			if (node1_id == node2_id) { continue; }

			Node node2(node2_id);
			Cluster cluster2(GetClusterAssignmentForNode(node2));
			if (graph_.AreNeighbours(node1, node2))
			{
				cluster_to_cluster_edges[cluster1.id][cluster2.id] += graph_.GetEdgeCost(node1, node2);
				cluster_to_cluster_iteract[cluster1.id][cluster2.id] = true;

				cluster_to_cluster_edges[cluster2.id][cluster1.id] += graph_.GetEdgeCost(node1, node2);
				cluster_to_cluster_iteract[cluster2.id][cluster1.id] = true;
			}
			else
			{
				cluster_to_cluster_edges[cluster1.id][cluster2.id] -= (graph_.GetNodeWeight(node1) * graph_.GetNodeWeight(node2));
				cluster_to_cluster_edges[cluster2.id][cluster1.id] -= (graph_.GetNodeWeight(node1) * graph_.GetNodeWeight(node2));
			}
		}
	}

	for (int i = 0; i < graph_.NumNodes(); i++)
	{
		for (int j = 0; j < graph_.NumNodes(); j++)
		{
			cluster_to_cluster_edges[i][j] /= 2;
		}
	}


	for (int i = 0; i < cluster_to_nodes.size(); i++)
	{
		if (cluster_to_nodes[i].empty()) { continue; }

		for (int j = i + 1; j < cluster_to_nodes.size(); j++)
		{
			if (cluster_to_cluster_edge_interaction[i].count(j))
			{
				int a = cluster_to_cluster_edges[i][j];
				int b = cluster_to_cluster_edge_interaction[i][j];
				runtime_assert(a == b);
				int k = 0;
			}			
		}
	}*/

	//clusters that do not interact directly with each other do not need their weights explicitly stored
	//	their edges can be omit and simply computed using their respective node weights

	/*DirectlyHashedIntegerSet clusters_used(state_.graph_.NumNodes());
	for (int node_id = 0; node_id < state_.graph_.NumNodes(); node_id++)
	{
		Cluster cluster = state_.GetClusterAssignmentForNode(Node(node_id));
		clusters_used.Insert(cluster.id);
	}*/

	int num_clusters = 0;
	std::vector<Cluster> cluster_to_new_cluster(graph_.NumNodes(), Cluster(-1));
	for (int node_id = 0; node_id < graph_.NumNodes(); node_id++)
	{
		Cluster cluster = GetClusterAssignmentForNode(Node(node_id));
		if (cluster_to_new_cluster[cluster.id] == Cluster(-1))
		{
			cluster_to_new_cluster[cluster.id] = Cluster(num_clusters++);
		}
	}

	//update the solution to the original instance
	for (int node_id = 0; node_id < original_node_to_current_node_.size(); node_id++)
	{
		Cluster c = original_node_to_current_node_[node_id];
		Cluster c2 = node_to_cluster_[c.id];
		original_node_to_current_node_[node_id] = cluster_to_new_cluster[c2.id];
	}

	std::vector<std::vector<PairNodeEdgeWeight> > new_adjacency_lists(num_clusters);
	for (int cluster1_id = 0; cluster1_id < cluster_to_cluster_edge_interaction.size(); cluster1_id++)
	{
		Cluster cluster1_new = cluster_to_new_cluster[cluster1_id];
		for (auto& pair_cluster_edge : cluster_to_cluster_edge_interaction[cluster1_id])
		{
			Cluster cluster2_new = cluster_to_new_cluster[pair_cluster_edge.first];
			int weight = pair_cluster_edge.second;

			new_adjacency_lists[cluster1_new.id].push_back(PairNodeEdgeWeight(Node(cluster2_new.id), weight));
		}
	}

	std::vector<int> new_node_weights(num_clusters);
	for (int node_id = 0; node_id < graph_.NumNodes(); node_id++)
	{
		Node node(node_id);
		Cluster cluster = GetClusterAssignmentForNode(node);
		Cluster new_cluster = cluster_to_new_cluster[cluster.id];
		new_node_weights[new_cluster.id] += graph_.GetNodeWeight(node);
	}

	graph_ = Graph(new_adjacency_lists, new_node_weights);
	/*for (auto& v1 : new_adjacency_lists)
	{
		for (auto& v2 : v1)
		{
			if (v2.edge_weight < 0)
			{
				constant_term_ -= v2.edge_weight;
			}
		}
	}*/
	node_to_cluster_.clear();
	node_weights_for_cluster_.clear();
	for (int cluster_id = 0; cluster_id < num_clusters; cluster_id++)
	{
		node_to_cluster_.push_back(Cluster(cluster_id));
		node_weights_for_cluster_.push_back(new_node_weights[cluster_id]);
	}

	int initial_graph_cost = 0;
	for (int node1_id = 0; node1_id < graph_.NumNodes(); node1_id++)
	{
		Node node1(node1_id);
		Cluster cluster1(GetClusterAssignmentForNode(node1));
		for (const PairNodeEdgeWeight& neighbour : graph_.GetAdjacencyListForNode(node1))
		{
			initial_graph_cost += neighbour.edge_weight;
		}
	}
	initial_graph_cost /= 2;

	constant_term_ = current_cost_ - initial_graph_cost;	

	empty_clusters_.Clear();
}

void SolverState::MergeCoClusteredNodesSimple()
{
	if (graph_.NumNodes() >= 5000)
	{
		std::cout << "Error: implementation needs to be improved for this many nodes...\n";
		exit(1);
	}

	std::vector<std::vector<int> > cluster_to_cluster_edges(graph_.NumNodes(), std::vector<int>(graph_.NumNodes(), 0));
	std::vector<std::vector<bool> > cluster_to_cluster_iteract(graph_.NumNodes(), std::vector<bool>(graph_.NumNodes(), false));

	for (int node1_id = 0; node1_id < graph_.NumNodes(); node1_id++)
	{
		Node node1(node1_id);
		Cluster cluster1(GetClusterAssignmentForNode(node1));
		for (int node2_id = 0; node2_id < graph_.NumNodes(); node2_id++)
		{
			if (node1_id == node2_id) { continue; }

			Node node2(node2_id);
			Cluster cluster2(GetClusterAssignmentForNode(node2));
			if (graph_.AreNeighbours(node1, node2))
			{
				cluster_to_cluster_edges[cluster1.id][cluster2.id] += graph_.GetEdgeCost(node1, node2);
				cluster_to_cluster_iteract[cluster1.id][cluster2.id] = true;

				cluster_to_cluster_edges[cluster2.id][cluster1.id] += graph_.GetEdgeCost(node1, node2);
				cluster_to_cluster_iteract[cluster2.id][cluster1.id] = true;
			}
			else
			{
				cluster_to_cluster_edges[cluster1.id][cluster2.id] -= (graph_.GetNodeWeight(node1) * graph_.GetNodeWeight(node2));
				cluster_to_cluster_edges[cluster2.id][cluster1.id] -= (graph_.GetNodeWeight(node1) * graph_.GetNodeWeight(node2));
			}
		}
	}

	for (int i = 0; i < graph_.NumNodes(); i++)
	{
		for (int j = 0; j < graph_.NumNodes(); j++)
		{
			cluster_to_cluster_edges[i][j] /= 2;
		}
	}


	//clusters that do not interact directly with each other do not need their weights explicitly stored
	//	their edges can be omit and simply computed using their respective node weights

	/*DirectlyHashedIntegerSet clusters_used(state_.graph_.NumNodes());
	for (int node_id = 0; node_id < state_.graph_.NumNodes(); node_id++)
	{
		Cluster cluster = state_.GetClusterAssignmentForNode(Node(node_id));
		clusters_used.Insert(cluster.id);
	}*/

	int num_clusters = 0;
	std::vector<Cluster> cluster_to_new_cluster(graph_.NumNodes(), Cluster(-1));
	for (int node_id = 0; node_id < graph_.NumNodes(); node_id++)
	{
		Cluster cluster = GetClusterAssignmentForNode(Node(node_id));
		if (cluster_to_new_cluster[cluster.id] == Cluster(-1))
		{
			cluster_to_new_cluster[cluster.id] = Cluster(num_clusters++);
		}
	}

	//update the solution to the original instance
	for (int node_id = 0; node_id < original_node_to_current_node_.size(); node_id++)
	{
		Cluster c = original_node_to_current_node_[node_id];
		Cluster c2 = node_to_cluster_[c.id];
		original_node_to_current_node_[node_id] = cluster_to_new_cluster[c2.id];
	}

	std::vector<std::vector<PairNodeEdgeWeight> > new_adjacency_lists(num_clusters);
	for (int cluster1_id = 0; cluster1_id < cluster_to_cluster_edges.size(); cluster1_id++)
	{
		Cluster cluster1_new = cluster_to_new_cluster[cluster1_id];
		for (int cluster2_id = cluster1_id + 1; cluster2_id < cluster_to_cluster_edges.size(); cluster2_id++)
		{
			if (cluster_to_cluster_iteract[cluster1_id][cluster2_id] == false) { continue; }

			Cluster cluster2_new = cluster_to_new_cluster[cluster2_id];
			int weight = cluster_to_cluster_edges[cluster1_id][cluster2_id];

			new_adjacency_lists[cluster1_new.id].push_back(PairNodeEdgeWeight(Node(cluster2_new.id), weight));
			new_adjacency_lists[cluster2_new.id].push_back(PairNodeEdgeWeight(Node(cluster1_new.id), weight));
		}
	}

	std::vector<int> new_node_weights(num_clusters);
	for (int node_id = 0; node_id < graph_.NumNodes(); node_id++)
	{
		Node node(node_id);
		Cluster cluster = GetClusterAssignmentForNode(node);
		Cluster new_cluster = cluster_to_new_cluster[cluster.id];
		new_node_weights[new_cluster.id] += graph_.GetNodeWeight(node);
	}

	graph_ = Graph(new_adjacency_lists, new_node_weights);
	for (auto& v1 : new_adjacency_lists)
	{
		for (auto& v2 : v1)
		{
			if (v2.edge_weight < 0)
			{
				constant_term_ -= v2.edge_weight;
			}
		}
	}
	node_to_cluster_.clear();
	node_weights_for_cluster_.clear();
	for (int cluster_id = 0; cluster_id < num_clusters; cluster_id++)
	{
		node_to_cluster_.push_back(Cluster(cluster_id));
		node_weights_for_cluster_.push_back(new_node_weights[cluster_id]);
	}

	constant_term_ = current_cost_ - ComputeCostFromScratchForGraph(graph_, node_to_cluster_);
	empty_clusters_.Clear();
}

int SolverState::ComputeCostFromScratchForGraph(const Graph graph, const std::vector<Cluster>& node_to_cluster)
{
	//todo: not the most efficient way to do this, could consider improving

	int cost = 0;
	for (int node1_id = 0; node1_id < graph.NumNodes(); node1_id++)
	{
		Node node1(node1_id);
		Cluster cluster1 = node_to_cluster[node1.id];
		for (int node2_id = node1_id+1; node2_id < graph.NumNodes(); node2_id++)
		{
			Node node2(node2_id);
			Cluster cluster2 = node_to_cluster[node2.id];

			bool are_neighbours = graph.AreNeighbours(node1, node2);

			if (are_neighbours && cluster1 != cluster2)
			{
				cost += graph.GetEdgeCost(node1, node2);
			}
			else if (!are_neighbours && cluster1 == cluster2)
			{
				cost += (graph.GetNodeWeight(node1) * graph.GetNodeWeight(node2));
			}
		}		
	}
	return cost;
}
