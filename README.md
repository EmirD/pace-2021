# README #

The repository holds the code used for the 'Kanpai' submission in the heuristic track of the PACE2021 challenge (cluster editing).

The algorithm is a bottom-up approach (hence the name). Initially each node is initially placed in their own isolated connected component, and iteratively nodes are combined into larger connected components in a greedy fashion. Once a local minima is reached, nodes in the same connected components are merged into super-nodes, and the process iterates. 

The algorithm reaches a local minima extremely quickly, e.g., for most benchmarks this happens within a second. They key point is that the best local move for a node can be computed in linear time with respect to the number of neighbours. The merge operation that takes place after a local minima is found plays a minor role, and in many cases it does not lead to improvements.

Majority of the improvements (99%+) are done within the first few seconds for most benchmarks. After that, the algorithm mainly repeates the procedure, using randomness to achieve slightly different solutions. Based on the current available results, the algorithms reaches the best solution for most benchmarks, whereas for the larger benchmarks the difference is less than one procent.

For more details please refer to the submission description given in the repository (kanpai-pace2021.pdf).

### Running the algorithm ###

Windows users can run the algorithm using the Visual Studio project provided in the repository. Linux users may compile by running "make all" in the Release folder. The algorithm can be run as in the competition, but also can be compiled to read command line arguments as usually expected (see the main.cpp for more details).

For any further questions please contact me at e.demirovic@tudelft.nl
